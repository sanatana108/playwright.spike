const glob = require('glob');
const path = require('path');
const justClone = require('just-clone');

const readPatternFile = (file) => {
  return require(`../../${file}`);
}

const patternFilesGlobPattern = './cucumber/step-definitions/patterns/**/*.pattern.js';

describe('Test patters', () => {
  glob.sync(patternFilesGlobPattern).forEach((file) => {
    const patternFile = readPatternFile(file);
    const baseFile = path.basename(file);

    it(`File ${baseFile} has all components`, () => {
      testPatternComponents(patternFile);
    });

    it(`File ${baseFile} has examples`, () => {
      testExamples(patternFile, baseFile);
    });

    it(`File ${baseFile} has minimal example`, () => {
      testMinExamples(patternFile, baseFile);
    });

    it(`File ${baseFile} has correct examples`, () => {
      testRegexExample(patternFile, baseFile);
    });

    it(`File ${baseFile} has correct minimal examples`, () => {
      testRegexMinExample(patternFile, baseFile);
    });

    it(`File ${baseFile} has usage`, () => {
      testUsage(patternFile, baseFile);
    });
  });

  it('Regex rules have no duplicates', () => {
    testPatternConflicts();
  });

  it('Check for regex conflicts', () => {
    testIfRegexMatchesOtherExamples();
  });

  const testPatternComponents = (patternObject) => {
    expect(patternObject).toHaveProperty('patterns');
    expect(patternObject).toHaveProperty('minimumExample');
    expect(patternObject).toHaveProperty('examples');
    expect(patternObject).toHaveProperty('usage');
  }

  const testMinExamples = (patternObject, filename) => {
    const patterns = patternObject.patterns;
    const patternNames = Object.keys(patterns);
    let patternName;
    let minExamples;

    expect(patternNames.length).toBeGreaterThanOrEqual(1);

    for (let key in patternNames) {
      patternName = patternNames[key];
      minExamples = patternObject.minimumExample[patternName] || '';
      try {
        expect(typeof minExamples).toBe('string');
      } catch (e) {
        throw new Error (`Pattern ${patternName} in ${filename} has no minimum example (not a string)`);
      }

      try {
        expect(minExamples.length).toBeGreaterThanOrEqual(1);
      } catch (e) {
        throw new Error (`Pattern ${patternName} in ${filename} has no minimum example (length)`);
      }
    }

    expect(Object.keys(patternObject.minimumExample).length).toBe(patternNames.length);
  }

  const testExamples = (patternObject, filename) => {
    const patterns = patternObject.patterns;
    const patternNames = Object.keys(patterns);
    let patternName;
    let examples;

    expect(patternNames.length).toBeGreaterThanOrEqual(1);

    for (let key in patternNames) {
      patternName = patternNames[key];
      examples = patternObject.examples[patternName] || [];
      try {
        expect(examples.length).toBeGreaterThanOrEqual(1);
      } catch (e) {
        throw new Error (`Pattern ${patternName} in ${filename} has no example`);
      }
    }
  }

  const testUsage = (patternObject, filename) => {
    const patterns = patternObject.patterns;
    const patternNames = Object.keys(patterns);
    let patternName;
    let usage;

    expect(patternNames.length).toBeGreaterThanOrEqual(1);

    const testIsUsageArray = (usage, patternName) => {
      try {
        expect(Array.isArray(usage)).toBe(true);
        expect(usage.length).toBeGreaterThanOrEqual(1);
      } catch (e) {
        throw new Error (`Pattern ${patternName} in ${filename} has no usage defined`);
      }
    }

    const testUsageValues = (usage, patternName) => {
      for (let key in usage) {
        const method = usage[key];
        try {
          expect(['Given', 'Then', 'And', 'When'].includes(method)).toBe(true);
        } catch (e) {
          throw new Error(`${method} is not a valid usage method for ${patternName} in ${filename}`)
        }
      }
    }

    for (let key in patternNames) {
      patternName = patternNames[key];
      usage = patternObject.usage[patternName] || [];
      testIsUsageArray(usage, patternName);
      testUsageValues(usage, patternName);
    }

    expect(Object.keys(patternObject.usage).length).toBe(patternNames.length);
  }

  const testRegexExample = (patternObject, filename) => {
    const patterns = patternObject.patterns;
    const patternNames = Object.keys(patterns);
    let patternName;
    let examples;

    expect(patternNames.length).toBeGreaterThanOrEqual(1);

    const testIsExampleArray = (examples, patternName) => {
      try {
        expect(Array.isArray(examples)).toBe(true);
      } catch (e) {
        throw new Error(`Examples for ${patternName} in ${filename} is not Array`);
      }

      try{
        expect(examples.length).toBeGreaterThanOrEqual(1);
      } catch (error) {
        throw new Error(`Examples for ${patternName} in ${filename} has no valid examples`);
      }
    }

    for (let key in patternNames) {
      patternName = patternNames[key];
      examples = patternObject.examples[patternName] || [];

      testIsExampleArray(examples, patternName);

      for (let exampleKey in examples) {
        testExample(patterns[patternName], examples[exampleKey], patternName, filename);
      }
    }
  }

  const testRegexMinExample = (patternObject, filename) => {
    const patterns = patternObject.patterns;
    const patternNames = Object.keys(patterns);
    let patternName;
    let minExamples;

    expect(patternNames.length).toBeGreaterThanOrEqual(1);

    for (let key in patternNames) {
      patternName = patternNames[key];
      minExamples = patternObject.minimumExample[patternName] || null;
      testExample(patterns[patternName], minExamples, patternName, filename);
    }
  }

  const testExample = (regex, example, patternName, filename) => {
    try {
      expect(regex.test(example)).toBe(true);
    } catch (e) {
      throw new Error (`Example -> ${example} () <- does not match -> ${regex.toString()} <- for ${patternName} in ${filename}`);
    }
  }

  const testPatternConflicts = () => {
    const flattenedPatterns = [];

    const allPatterns = [];
    glob.sync(patternFilesGlobPattern).forEach((file) => {
      const patternFile = readPatternFile(file).patterns;
      allPatterns.push(patternFile);
    });

    const extractPatterns = (patternGroup) => {
      const patterns = patternGroup.patterns;
      for (let key in patterns) {
        const extractPattern = patterns[key];
        const regexString = extractPattern.toString().toLowerCase();

        if (flattenedPatterns.includes(regexString)) {
          throw new Error(`${regexString} already exists`);
          continue;
        }

        flattenedPatterns.push(regexString);
      }
    }

    expect(Array.isArray(allPatterns)).toBe(true);
    expect(allPatterns.length).toBeGreaterThanOrEqual(1);

    for (let key in allPatterns) {
      extractPatterns(allPatterns[key]);
    }
  }

  const testIfRegexMatchesOtherExamples = () => {
    const allPatterns = [];

    glob.sync(patternFilesGlobPattern).forEach((file) => {
      const patternFile = readPatternFile(file);
      const baseName = path.basename(file);
      allPatterns.push({ baseName,
        patterns: patternFile.patterns,
        minimumExample: patternFile.minimumExample,
        examples: patternFile.examples,
      });
    });

    const getOtherExamples = (patternName, baseName) => {
      let patterns = justClone(allPatterns);

      let examples = [];

      for (let key in patterns) {
        if (patterns[key].baseName === baseName) {
          delete patterns[key].examples[patternName];
          delete patterns[key].minimumExample[patternName];
        }

        for (let exampleKey in patterns[key].examples) {
          examples = [ ...examples, ...patterns[key].examples[exampleKey] || []];
        }
      }

      return examples;
    }

    const compareToOtherExamples = (patternName, baseName, regex) => {
      const examples = getOtherExamples(patternName, baseName);
      const errors = [];
      let count = 0;
      examples.forEach((item) => {
        if (regex.test(item)) {
          count += 1;
          errors.push(`${count}. ${item} --> ${regex.toString()}`);
        }
      });

      try {
        expect(errors.length).toBe(0);
      } catch (e) {
        throw new Error(`Some of the examples match other regex rules: \n\n${ Object.values(errors).join("\n") }\n`);
      }
    }

    for (let key in allPatterns) {
      const baseName = allPatterns[key].baseName;
      const patterns = allPatterns[key].patterns;
      for (let patternName in patterns) {
        compareToOtherExamples(patternName, baseName, patterns[patternName]);
      }
    }
  }
});
