/**
 * @class
 * @type {BaseController}
 */
module.exports = class BaseController {

  all(req, res, next) {
    switch (req.method) {
      case 'POST':
        this.doPost(req, res, next);
        break;

      case 'GET':
        this.doGet(req, res, next);
        break;

      case 'PUT':
        this.doPut(req, res, next);
        break;

      case 'DELETE':
        this.doDelete(req, res, next);
        break;

      case 'PATCH':
        this.doPatch(req, res, next);
        break;

      default:
        this.sendForbiddenResponse(req, res);
        break;
    }
  }

  sendForbiddenResponse = (req, res) => res.status(405).json({
    status: 405,
    message: `Method ${req.method} is not allowed on requested endpoint`,
  })

  doGet(req, res) {
    return this.sendForbiddenResponse(req, res);
  }

  doPost(req, res) {
    return this.sendForbiddenResponse(req, res);
  }

  doPut(req, res) {
    return this.sendForbiddenResponse(req, res);
  }

  doDelete(req, res) {
    return this.sendForbiddenResponse(req, res);
  }

  doPatch(req, res) {
    return this.sendForbiddenResponse(req, res);
  }
};
