const BaseController = require('../base.controller');

/**
 * @function
 * @type HomeController
 */
class HtmlDocsController extends BaseController {
  documentsService;

  /**
   *
   * @param {DocumentsService} documentsService
   */
  constructor(documentsService) {
    super();
    this.documentsService = documentsService;
  }

  doGet = (req, res) => {
    return res.send(this.documentsService.getHtml());
  }
}

module.exports = HtmlDocsController;
