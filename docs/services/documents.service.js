const glob = require('glob');
const path = require("path");

class DocumentsService {
  interpreterCollection = [];

  constructor() {
    let interpreter;
    let name;

    glob.sync('./cucumber/step-definitions/**/*.pattern.js').forEach((file) => {
      interpreter = require(path.resolve(file));
      name = this.convertPatternName(path.basename(file));
      this.interpreterCollection.push( {
        name,
        patterns: this.convertPatternsToObject(interpreter),
      });
    });
  }

  getPatterns = () => {
    return this.interpreterCollection;
  }

  convertPatternName = (text) => {
    const result = text.replace(/\.pattern\.js/, '').replace(/([A-Z])/g, " $1");
    return `${result.charAt(0).toUpperCase()}${result.slice(1)}`;
  }

  convertPatternsToObject = (interpreter) => {
    const patternCollection = [];

    for (const [key, value] of Object.entries(interpreter.patterns)) {
      patternCollection.push(
        {
          id: key,
          pattern: value.toString(),
          minExample: interpreter.minimumExample[key],
          examples: interpreter.examples[key].concat([interpreter.minimumExample[key]]),
          usage: interpreter.usage[key],
        }
      );
    }

    return patternCollection;
  }

  htmlEntities = (str) => {
    return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
  }

  getHtml = () => {
    const allPatterns = this.getPatterns();
    let html = '';

    allPatterns.forEach((item) => {
        html += `<h2>${item.name}</h2>`;

        item.patterns.forEach((pattern) => {
          html += 'Regex pattern:';
          html += `<h3>${this.htmlEntities(pattern.pattern)}</h3>`;
          html += `Examples:`;
          html += '<ul>';
          pattern.examples.forEach((example) => {
            html += `<li>${example}</li>`;
          });
          html += '</ul>';
        });

        html += '<hr>';
    });

    return html;
  }
}

module.exports = DocumentsService;
