class BaseApi {
  app;
  services;

  constructor(app) {
    this.app = app;
    this.app.disable('x-powered-by');
    this.app.disable('access-control-allow-methods');
  }

  start(port) {
    const usePort = port || 3000;
    console.log(`Api started on port ${usePort}!`);
    this.server = this.app.listen(usePort);
  }
}

module.exports = BaseApi;
