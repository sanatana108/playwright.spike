const HtmlDocumentsController = require('../../controllers/documents/html.controller');

module.exports = ((app, services) => {
  function getHomeController() {
    return new HtmlDocumentsController(services.get('documents'));
  }

  app.all('/', (req, res, next) => {
    const RouteController = getHomeController();
    RouteController.all(req, res, next);
  });
});
