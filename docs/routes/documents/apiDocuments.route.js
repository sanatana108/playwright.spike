const DocumentsController = require('../../controllers/documents/documents.controller');

module.exports = ((app, services) => {
  function getHomeController() {
    return new DocumentsController(services.get('documents'));
  }

  app.all('/api', (req, res, next) => {
    const RouteController = getHomeController();
    RouteController.all(req, res, next);
  });
});
