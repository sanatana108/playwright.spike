const Api = require('./app');

const api = new Api();
api.start(process.env.APP_PORT || 3001);