const common = `
  --require-module ts-node/register 
  --require cucumber/test.setup.ts 
  --require cucumber/step-definitions/**/*.interpreter.ts
  --require-module ts-node/register
  --format json:results/reports/report.json 
  --format message:results/reports/report.ndjson
  --format html:results/reports/report.html
  --format summary 
  --format progress-bar 
  --format @cucumber/pretty-formatter
  --format-options ${JSON.stringify({ snippetInterface: 'async-await' })}
  --publish-quiet
  `;

const getWorldParams = () => {
  const params = {}
  return `--world-parameters ${JSON.stringify({ params })}`;
};

module.exports = {
  default: `${common} ${getWorldParams()}`,
};
