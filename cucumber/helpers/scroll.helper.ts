import { Page } from 'playwright';

export async function scrollFullPage(page: Page) {
  return page.evaluate(() => {
    return new Promise<void>((resolve, reject) => {
      window.scrollTo(0,0);
      let totalHeight = 0;
      const distance = 200;
      const timer = setInterval(() => {
        const scrollHeight = document.body.scrollHeight;
        window.scrollBy(0, distance);
        totalHeight += distance;

        if(totalHeight >= scrollHeight){
          clearInterval(timer);
          resolve();
        }
      }, 100);
    })
  });
}

