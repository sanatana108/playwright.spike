import {
  devices,
} from 'playwright';

const getRequestedDeviceId = () : null|string => {
  const deviceRegex = /device=(.*?)\b$/i;
  let deviceId = null;
  for (const key in process.argv) {
    if (deviceRegex.test(process.argv[key])) {
      const match = process.argv[key].match(deviceRegex);
      if (match && match[1]) {
        deviceId = match[1];
        break;
      }
    };
  }

  return deviceId;
}

export const getRequestedDevice = () : null|{ isMobile: boolean, name?: string|null }=> {
  let device = { isMobile: false, name: null };
  const deviceId = getRequestedDeviceId();

  if (!deviceId) {
    return device;
  }

  if (devices[deviceId]) {
    return { ...device, ...devices[deviceId], name: deviceId };
  }

  return device;
}

export const getDefaultViewportSize = () : { viewport: { width: number, height: number }} => {
  return {
    viewport: {
      width: 1900,
      height: 1000,
    }
  };
}
