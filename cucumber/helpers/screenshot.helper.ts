// @ts-nocheck

import path from 'path';
import { ensureDir } from 'fs-extra';
import { Page } from 'playwright';
import fs from 'fs';
import joinImages from 'join-images';
import * as Buffer from "buffer";

const screenshotOptions = { fullPage: false };
const screenShotsFileExtension = 'png';

export const getScreenshotName = async(filename: string, increment:number = 0): Promise<string> => {
    const incrementString = increment ? ` (${increment})` : '';
    const directory = path.dirname(filename);
    await ensureDir(directory);
    const extension = getValidExtension(path.extname(filename) || '.png');
    const safeBaseName = getBaseSafeName(filename);
    const name = `${directory}/${safeBaseName}${incrementString}${extension}`;

    if (fs.existsSync(name)) {
        return await getScreenshotName(filename,increment += 1);
    }
    return name;
}

export const isFullPage = (wholePage: string|null) : boolean => {
    if (typeof wholePage === 'string') {
        return ['fullpage'].includes(wholePage.toString().toLowerCase().replace(' ', ''));
    }
    return false;
}

export const getIncrementFromFileName = (filename: string) => {
    const matches = filename.match(/ \((\d+?)\)\./);
    if (matches && matches[1]) {
        return matches[1];
    }
    return 0;
}

export async function screenshotScrollFullPage(page: Page, filename: string, override: boolean = false, timeout: number|null = 0) {
    let grabs: Buffer[] = [];
    let count = 0;
    const userDefinedFilename = filename;
    let previousDimensions = new PreviousDimensions();

    const getFileName = function() {
        return userDefinedFilename;
    }

    const getOverride = function() {
        return override;
    }

    const getGrabs = function() {
        return grabs;
    }

    const reset = async function() {
        grabs = [];
        count = 0;
        previousDimensions = new PreviousDimensions();
    }

    const addScreenShot = (screenshot: Buffer|null) => {
        if (screenshot) {
            grabs.push(screenshot);
        }
    }

    const isFunctionExposed = async (name: string) : Promise<boolean> => {
        return await page.evaluate((name) => {
            // @ts-ignore
            return typeof window[name] === 'function';
        }, name);
    }


    const doSaveAllScreenShots = async function(options) {
        await saveFullPageScreenShot(getGrabs(), options.filename, options.override);
        await reset();
        await sleep(500);
    };

    const playwrightScrollFinished = `playwrightScrollFinished`;
    if (!await isFunctionExposed(playwrightScrollFinished)) {
        await page.exposeFunction(playwrightScrollFinished, doSaveAllScreenShots);
    } else {
        await reset();
    }

    const playwrightTakeScreenShot = 'playwrightTakeScreenShot';
    if (!await isFunctionExposed(playwrightTakeScreenShot)) {
        await page.exposeFunction(playwrightTakeScreenShot, async function (options: any) {
            const screenshot = await takeScreenShot(options);
            if (screenshot) {
                addScreenShot(screenshot);
            }
        });
    }

    const takeScreenShot = async function (options: PreviousDimensions) {
        count += 1;

        const clipOptions = getClippingOptions(options);
        let screenshot = null;
        try {
            screenshot = await page.screenshot({ ...screenshotOptions, ...clipOptions }, { waitUntil: 'networkidle' });
        } catch (e) {
            console.log(e);
        }

        previousDimensions = new PreviousDimensions({
            start: options.start,
            end: options.end,
            height: options.height,
            width: options.width
        });
        return screenshot;
    };

    const getClippingOptions = function(options: PreviousDimensions) : null|{ clip: {
        x: number,
        y: number,
        width: number,
        height: number
    }} {

        const currentPosition = previousDimensions.start + previousDimensions.height;

        let clipOptions = {};
        if (currentPosition > options.start) {
            clipOptions = {
                clip: {
                    x: 0,
                    y: currentPosition - options.start,
                    width: options.width,
                    height: options.height,
                }
            }
        }
        return clipOptions;
    };

    await reset();
    return page.evaluate((settings: any) => {
        return new Promise<void>(async (resolve, reject) => {
            const ScreenShotScroller = function (options, resolveMethod) {

                this.timeoutBetweenSteps = options.timeout;
                this.resolveMethod = resolveMethod;
                this.settings = options;

                this.getCurrentYPosition = function () {
                    return document.documentElement.scrollTop || document.body.scrollTop;
                };

                this.sleep = function (ms) {
                    return new Promise((resolve, reject) => {
                        setTimeout(resolve, ms);
                    });
                };

                this.getDimensions = function () {
                    const currentPosition = this.getCurrentYPosition();
                    const distance = this.getDistance();
                    const options = {
                        start: currentPosition,
                        end: distance + currentPosition,
                        width: window.innerWidth,
                        height: distance,
                    }
                    return options;
                };

                this.scrollTo = function (offset, callback) {
                    const currentPosition = document.documentElement.scrollTop || document.body.scrollTop;
                    const destination = currentPosition + offset;
                    const that = this;

                    const reachedDestination = function () {
                        return (document.documentElement.scrollTop || document.body.scrollTop) === destination;
                    }

                    const onScrollPlaywright = function () {
                        if (that.isAtTheBottom() || reachedDestination()) {
                            window.removeEventListener('scroll', onScrollPlaywright);
                            setTimeout(function () {
                                callback();
                            }, that.timeoutBetweenSteps);
                        }
                    }

                    window.addEventListener('scroll', onScrollPlaywright)
                    onScrollPlaywright();
                    window.scrollBy({
                        top: offset,
                    })
                };

                this.getDistance = function () {
                    return window.innerHeight;
                };

                this.isAtTheBottom = function () {
                    return window.innerHeight + (window.pageYOffset || window.scrollY) >= document.body.offsetHeight;
                };

                this.takeScreenShot = async function () {
                    try {
                        await window.playwrightTakeScreenShot(this.getDimensions());
                    } catch (e) {
                        console.log(e);
                    }
                };

                this.scrollToBottom = async function () {
                    await this.takeScreenShot();
                    await this.sleep(this.timeoutBetweenSteps);
                    await this.continueScroll();
                };

                this.continueScroll = async function () {
                    var that = this;
                    this.scrollTo(this.getDistance(), async function () {
                        const bottomReached = that.isAtTheBottom();
                        if (!bottomReached) {
                            await that.scrollToBottom(that);
                        } else {
                            await that.takeScreenShot(that);
                            await that.sleep(that.timeoutBetweenSteps);

                            that.resolveMethod(await window.playwrightScrollFinished({
                                filename: that.settings.filename,
                                override: that.settings.override,
                            }));
                        }
                    });
                };

                return this;
            }

            // @ts-ignore
            window.scrollTo(0,0);
            if (typeof ScreenShotScroller !== 'undefined') {
                const scroller = new ScreenShotScroller(settings, resolve);
                setTimeout(function () {
                    scroller.scrollToBottom();
                }, 1000); //please wait a bit otherwise for some reason things get messy.
            }
        });

    }, { timeout, filename, override });
}

const sleep = function (ms) {
    return new Promise((resolve, reject) => {
        setTimeout(resolve, ms);
    });
}

const deleteScreenShotFile = async (filename: string, retryCount: number = 0) => {
    if (!fs.existsSync(filename)) {
        return true;
    }

    if (retryCount > 10) {
        throw new Error(`${filename} can not be removed`);
    }

    try {
        await fs.unlinkSync(filename);
    } catch (e) {
        await sleep(1000);
        return deleteScreenShotFile(filename, retryCount += 1);
    }

    return true;
}

export const saveFullPageScreenShot = async (data: Buffer[], filename: string, override: boolean) : Promise<string> => {

    if (override ) {
        await deleteScreenShotFile(filename);
    }

    let destinationFilename = filename;
    if (!override) {
        destinationFilename = await getScreenshotName(filename);
    }

    return joinImages(data, { margin: 0, offset: 0, direction: 'vertical' })
        .then(async (finalImageData: any) => {
            return await finalImageData.toFile(destinationFilename);
        }).then(() => {
            return destinationFilename;
        }).catch((err: Error) => {
            console.log(err);
            throw new Error('Full page screenshot can\'t be saved: ' + err.toString());
        });
}

const getBaseSafeName = (filename: string) : string => {
    return path.basename(filename, path.extname(filename))
        .replace(/[^A-z0-9\-_]/gi, '_')
        .toLowerCase();
}

const isValidExtension = (extension: string) : boolean => {
    return ['png'].includes(extension);
}

const getValidExtension = (extension: string|null) : string => {
    const defaultExtension = '.png';
    if (!extension) {
        return defaultExtension;
    }

    const checkExtension = extension.toLowerCase();
    if (!isValidExtension(checkExtension)) {
        return defaultExtension;
    }

    return checkExtension;
}

export const getResolution = async (page: Page) : Promise<{ width: number, height: number }> => {
    return page.evaluate(() => {
        const vw = Math.max(document.documentElement.clientWidth || 0, window.innerWidth || 0)
        const vh = Math.max(document.documentElement.clientHeight || 0, window.innerHeight || 0)
        return { width: vw, height: vh };
    });
}

export const safeFileNameCharacters = (name: string): string => {
    return name.replace(/[^A-z0-9_\-\. \[\]\(\)=]+/gi, '-');
}

export const getScreenShotBaseFilename = async (world: any, options?: ScreenShotNamesOptions) : Promise<string> => {

    let { res, browserName, deviceName, testBasename, selectorName } = await getScreenshotNameParts(world, options);

    let nameParts = [
        testBasename,
        browserName,
        deviceName,
        selectorName
    ];

    nameParts = nameParts.filter((word) => {
        return word && word.length > 0;
    });

    const filename = nameParts.join('-') +
        `-${res.width}x${res.height}.${screenShotsFileExtension}`;

    return filename;
}

const getScreenshotNameParts = async (world: any, options?: ScreenShotNamesOptions) => {

    let res = { width: 0, height: 0 };
    let browserName = 'unknown-browser';
    let deviceName = 'desktop';
    let testBasename = 'unknown-test';
    let selectorName = '';

    if (world.page) {
        res = await getResolution(world.page);
    }

    if (world.browser && world.browser._name) {
        browserName = (world.browser._name || browserName).toLowerCase();
    }

    if (world.device && world.device.name) {
        deviceName = `[${safeFileNameCharacters(world.device.name)}]`.toLowerCase();
    }

    if (world.testBaseName) {
        testBasename = (world.testBaseName).toLowerCase();
    }

    if (options && options.selector && typeof options.selector === 'string' && options.selector.length > 0) {
        selectorName = `[${safeFileNameCharacters(options.selector)}]`.toLowerCase();
    }

    if (options && options.name && typeof options.name === 'string' && options.name.length > 0) {
        testBasename = `${safeFileNameCharacters(options.name)}`.toLowerCase();
    }

    return { res, browserName, deviceName, testBasename, selectorName };
}

export default {};

class PreviousDimensions {
    start = 0;
    end = 0;
    height = 0;
    width = 0;

    constructor(options?: { start?: number, end?: number, height?: number, width?: number }) {
        this.start = options?.start ? options.start : 0;
        this.end = options?.end ? options.end : 0;
        this.height = options?.height ? options.height : 0;
        this.width = options?.width ? options.width : 0;
    }
}


interface ScreenShotNamesOptions {
    name?:string,
    selector?:string
}
