export function isUrl (url: string) {
    const isUrl = new RegExp('^https?:');
    return isUrl.test(url);
}

export default { }
