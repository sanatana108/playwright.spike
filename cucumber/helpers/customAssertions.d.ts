// copied into our codebase for autocompletion purposes from 'playwright/types/types.d.ts' so we don't depend on it.
import { Page } from 'playwright';

export interface PlaywrightMatchers<R> {
  isVisibleInViewport(
      page: Page,
      isPartiallyVisible?: boolean,
  ): Promise<R>;

  isNotVisibleInViewport(
      page: Page,
  ): Promise<R>;

  isVideoPlaying(
  ): Promise<R>;

  isVideoNotPlaying(
  ): Promise<R>;

  toMatchMySnapshot(
    snapshot: string,
    options?: object,
  ): Promise<R>;
}

export const customMatchers: any;

