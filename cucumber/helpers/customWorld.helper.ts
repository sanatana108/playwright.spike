import { setWorldConstructor, World, IWorldOptions } from '@cucumber/cucumber';
import * as messages from '@cucumber/messages';
import { BrowserContext, ChromiumBrowser, FirefoxBrowser, Page, WebKitBrowser } from 'playwright';

export interface CucumberWorldConstructorParams {
  parameters: { [key: string]: string };
}

export interface ICustomWorld extends World {
  debug: boolean;
  feature?: messages.Pickle;
  context?: BrowserContext;
  page: Page | null;
  testName?: string;
  expect?: any;
  browser: null | ChromiumBrowser | FirefoxBrowser | WebKitBrowser;
  lastScreenShot: { }|null,
  testBaseName: string,
  setTimeout?: Function,
  resetTimeout?: Function
  browserName: string,
  device: any|null,
  stepNumber: number,
}

export class CustomWorldHelper extends World implements ICustomWorld, CucumberWorldConstructorParams {
  page = null;
  debug = false;
  browser = null;
  lastScreenShot = null;
  testBaseName = '';
  browserName = '';
  device = null;
  stepNumber = 0;

  constructor(options: IWorldOptions) {
    super(options);
  }
}

setWorldConstructor(CustomWorldHelper);
