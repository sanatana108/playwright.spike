export function getCorrectCondition(condition: string) {

  if (typeof condition !== 'string') {
    condition = '';
  }

  switch (condition.toString().toLowerCase()) {
    case 'at least':
      return '>='
      break;

    case 'at most':
    case 'not more':
      return '<=';
      break;

    case 'only':
    case 'exactly':
    default:
      return '=';
      break;
  }
}

exports.default = {};