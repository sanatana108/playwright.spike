Object.defineProperty(exports, "__esModule", { value: true });
exports.customMatchers = void 0;
const customMatchers = require('./matchers');
exports.customMatchers = customMatchers.default;

// @ts-ignore
if (typeof global.expect !== "undefined") {
    // @ts-ignore
    global.expect.extend(customMatchers.default);
}
