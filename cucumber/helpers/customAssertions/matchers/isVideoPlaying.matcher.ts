Object.defineProperty(exports, "__esModule", { value: true });
import { Locator, Page } from 'playwright';
import { sleep } from '../../../helpers/time.helper';

const timeoutSeconds = 30;

const isVideoPlaying = async function (video: Locator, options = {}) {
  const isPlaying = async (video: Locator, count: number = 1): Promise<any> => {

    const playing = await video.evaluate((node: any) => {
      return node.currentTime > 0 && !node.paused && !node.seeking && !node.ended;
    });

    if (!playing && count < timeoutSeconds) {
      await sleep(1000);
      count += 1
      return isPlaying(video, count += 1);
    }

    return playing;
  }

  try {
    const playing = await isPlaying(video);

    return {
      pass: playing,
      message: () => 'Video is not playing',
    };
  }
  catch (err: any) {
    return {
      pass: false,
      message: () => err.toString(),
    };
  }
};

exports.default = isVideoPlaying;
