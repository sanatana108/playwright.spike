Object.defineProperty(exports, "__esModule", { value: true });
import { Locator, Page } from 'playwright';

const isNotVisibleInViewport = async function (locator: Locator, page: Page) {

  try {
    const isVisible = await isAnyPartInViewport(locator, page);
    return {
      pass: !isVisible,
      message: () => 'Element is visible in the viewport',
    };
  }
  catch (err: any) {
    return {
      pass: false,
      message: () => err.toString(),
    };
  }
};

exports.default = isNotVisibleInViewport;

const isAnyPartInViewport = async function (elem: Locator, page: Page) {
  const boundingBox = await elem.boundingBox();
  const viewPortBox = await page.viewportSize();

  if (!boundingBox || !viewPortBox) {
    return false;
  }

  const out = {
    top: boundingBox.y < 0,
    left: boundingBox.x < 0,
    bottom : boundingBox.y + boundingBox.height > viewPortBox.height,
    right: boundingBox.x + boundingBox.width > viewPortBox.width,
    any: false,
    all: false,
  }

  out.all = out.top && out.left && out.bottom && out.right;

  return out.all;
}
