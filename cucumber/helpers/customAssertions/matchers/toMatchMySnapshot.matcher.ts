Object.defineProperty(exports, "__esModule", { value: true });

import fs from 'fs';
import pixelmatch from 'pixelmatch';
const PNG = require('pngjs').PNG;
import joinImages from 'join-images';

const toMatchMySnapshot = async function (source: string, compareItWithSnapshot: string, options: any) {
    try {
        const isDifferent = await compareImages(source, compareItWithSnapshot, options);
        return {
            pass: options.compareOnly === true ? true: isDifferent === 0,
            message: () => `Screenshot does not match snapshot. See ${compareItWithSnapshot} for details`,
        };
    } catch (err: any) {
        return {
            pass: false,
            message: () => err.toString(),
        };
    }
};

exports.default = toMatchMySnapshot;

const compareImages = async (source: string, compareItWithSnapshot: string, options = {}): Promise<number> => {
    const defaultOptions = {
        direction: 'horizontal',
        color: 'black',
        offset: 2,
        filename: null,
        threshold: 0.1,
        margin: 0,
    };

    const matchOptions = {...defaultOptions, ...options};
    const sourceBuffer = fs.readFileSync(source);
    const screenshotImage = PNG.sync.read(sourceBuffer);

    if (!screenshotImage) {
        throw new Error('Source screenshot is not a string or buffer');
    }

    const snapshotBuffer = fs.readFileSync(compareItWithSnapshot);
    const snapshotImage = PNG.sync.read(snapshotBuffer);

    const width = Math.max(screenshotImage.width, screenshotImage.width);
    const height = Math.max(snapshotImage.height, snapshotImage.height);

    const diff = new PNG({width, height});

    let isDifferent = 1;
    //at the moment images must be same dimensions
    if (snapshotImage.width === screenshotImage.width && snapshotImage.height === screenshotImage.height) {
        isDifferent = pixelmatch(screenshotImage.data, snapshotImage.data, diff.data, width, height, {
            threshold: matchOptions.threshold ? matchOptions.threshold : 0.0
        });
    }

    if (!matchOptions.filename) {
        return isDifferent;
    }

    const diffTempImage = await joinImages([sourceBuffer, snapshotBuffer, PNG.sync.write(diff)], {
            margin: matchOptions.margin,
            color: matchOptions.color,
            direction: matchOptions.direction === 'horizontal' ? matchOptions.direction : 'vertical',
            offset: matchOptions.offset,
        }
    );

    diffTempImage.toFile(matchOptions.filename);
    return isDifferent;
}

