Object.defineProperty(exports, "__esModule", { value: true });
import { Locator, Page } from 'playwright';

const isVisibleInViewport = async function (locator: Locator, page: Page, isPartiallyVisible = true) {

  try {
    const elementVisibilityInViewport = await isAnyPartInViewport(locator, page);
    const isVisible = isPartiallyVisible ? elementVisibilityInViewport.any :
        await elementVisibilityInViewport.all;

    return {
      pass: await locator.count() && isVisible,
      message: () => 'Element is not visible in the viewport',
    };
  }
  catch (err: any) {
    return {
      pass: false,
      message: () => err.toString(),
    };
  }
};

exports.default = isVisibleInViewport;

const isAnyPartInViewport = async function (elem: Locator, page: Page) : Promise<any> {
  const boundingBox = await elem.boundingBox();
  const viewPortBox = await page.viewportSize();

  if (!boundingBox || !viewPortBox) {
    return false;
  }

  let inView = {
    top: boundingBox.y >= 0 && boundingBox.y <= viewPortBox.height,
    bottom: boundingBox.y + boundingBox.height <= viewPortBox.height && boundingBox.y + boundingBox.height >= 0,
    left: boundingBox.x >= 0,
    right: boundingBox.x + boundingBox.width >= 0 && boundingBox.x + boundingBox.width <= viewPortBox.width,
    any: false,
    all: false,
  }

  inView.all = inView.top && inView.bottom && inView.left && inView.right;
  inView.any = (inView.top || inView.bottom) && (inView.top || inView.right);
  return inView;
}
