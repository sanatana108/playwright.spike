const path = require('path');
const glob = require('glob');

var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};

Object.defineProperty(exports, "__esModule", { value: true });

let customMatchers = {};

glob.sync('./**/*.matcher.*').forEach((file) => {

    let matcherName = path.basename(file).replace(/(.*)\.matcher(.*)/i, function(_, firstMatch) {
        return firstMatch;
    });

    const matcher = require(path.resolve(file));
    customMatchers[matcherName] = matcher.default;
});

exports.default = customMatchers;
