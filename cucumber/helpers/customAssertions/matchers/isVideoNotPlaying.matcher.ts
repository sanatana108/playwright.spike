Object.defineProperty(exports, "__esModule", { value: true });
import { Locator, Page } from 'playwright';
import { sleep } from '../../../helpers/time.helper';

const timeoutSeconds = 30;

const isVideoNotPlaying = async function (video: Locator, options = {}) {

  try {
    const notPlaying = await isNotPlaying(video);

    return {
      pass: notPlaying,
      message: () => 'Video is playing',
    };
  }
  catch (err: any) {
    return {
      pass: false,
      message: () => err.toString(),
    };
  }
};

exports.default = isVideoNotPlaying;

const isNotPlaying = async (video: Locator, count: number = 1): Promise<any> => {
  const notPlaying = await video.evaluate((node: any) => {
    return node.currentTime === 0 || node.paused || node.ended;
  });

  if (!notPlaying && count < timeoutSeconds) {
    await sleep(1000);
    count += 1
    return isNotPlaying(video, count += 1);
  }

  return notPlaying;
}