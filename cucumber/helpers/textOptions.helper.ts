
export function escapeRegexString(string: string) {
  if (typeof string !== 'string') {
    throw new TypeError('Expected a string');
  }

 return string
      .replace(/[|\\{}()[\]^$+*?.]/g, '\\$&')
      .replace(/-/g, '\\x2d');
}

export const isMatchCase = (contentOption: string) => {
  let matchCase = ['casesensitive', 'keysensitive', 'sensitive', '', null, false, undefined].includes(contentOption);
  return matchCase;
};

export const getMatchRegex = (content: string, contentOptions: any) => {
  return new RegExp(escapeRegexString(content), isMatchCase(contentOptions) ? '' : 'i');
}

export default {};
