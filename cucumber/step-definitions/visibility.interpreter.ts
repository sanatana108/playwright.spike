import { Then } from '@cucumber/cucumber';
import { getMatchRegex } from '../helpers/textOptions.helper';

const {
    contentIsInElementPattern,
    textIsPresentPattern,
    contentIsNotPresentPattern,
    elementIsNotVisiblePattern,
    elementWithTextPattern,
    elementIsVisiblePattern,
} = require('./patterns/visibility.pattern').patterns;

// /I see(.*)?( \(.*\))?( with) (text|content) "(.*)"(\|(.*))
Then(elementWithTextPattern, async function(_, selector: string, _1, _2, searchContent: string, matchCaseOption: string) {
    const searchPattern = getMatchRegex(searchContent, matchCaseOption).toString();
    const textElement = await this.page?.locator(`${selector}:has-text=${searchPattern} >> visible=true`).first();
    await this.expect(textElement).toBeTruthy();
});

///I see (text|content) "(.*)"(\|(.*))? in( (.*))? \((.*)\)?/i
Then(contentIsInElementPattern, async function(_, searchContent: string, matchCaseOption: string|null, selector: string, _1) {
    const searchPattern = getMatchRegex(searchContent, matchCaseOption).toString();
    const textElement = await this.page?.locator(`${selector}:has-text=${searchPattern} >> visible=true`).first();
    await this.expect(textElement).toBeTruthy();
});

//const textIsPresentPattern = /I see (text|content) "(.*)"(\|(.*))?$/i;
Then(textIsPresentPattern, async function(_1, searchContent: string, matchCaseOption: string|null) {
    const searchPattern = getMatchRegex(searchContent, matchCaseOption).toString();
    const textElement = await this.page.locator(`text=${searchPattern} >> visible=true`);
    await this.expect(textElement).toBeTruthy();
});

// const contentIsNotPresentPattern = /^((Text|Content) )?"(.*)"(\|(\w+))? is not (present|visible)( (.*))?/i;
Then(contentIsNotPresentPattern, async function (_1, searchContent: string, matchCaseOption: string, _2, _3) {
    const searchPattern = getMatchRegex(searchContent, matchCaseOption).toString();
    const textElement = await this.page.locator(`text=${searchPattern} >> visible=true`);
    await this.expect(await textElement.count()).toBe(0);
});

// /(.*)?\((.*)\) is not visible(.*)(?<!viewport)$/i;
Then(elementIsNotVisiblePattern, async function (_1, selector: string, _2) {
    const textElement = await this.page.locator(`${selector} >> visible=true`);
    await this.expect(await textElement.count()).toBe(0);
});

// /(.*)?\((.*)\)(((?!(and text|and content| with text|with content)).)*)? is visible(.*)(?<!viewport)$/i;
Then(elementIsVisiblePattern, async function(_1, selector: string, _2, _3) {
    const textElement = await this.page.locator(`${selector} >> visible=true`).first();
    await this.expect(await textElement.count()).toBe(1);
});
