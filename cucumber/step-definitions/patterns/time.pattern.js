const waitPattern = /I wait ([0-9]+\.?[0-9]*|\.[0-9]+) second(s)?/i

module.exports = Object.freeze(
  {
    patterns: {
      waitPattern
    },
    minimumExample: {
      waitPattern: 'I wait 1 second'
    },
    examples: {
      waitPattern: [
        'I wait 1 second',
        'I wait 2 seconds',
        'I wait 10 seconds'
      ],
    },
    usage: {
      waitPattern: ['Then', 'And']
    }
  }
);