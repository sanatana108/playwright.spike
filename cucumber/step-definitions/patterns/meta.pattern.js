const titlePattern = /I see "(.*)"(\|(.*))? in( (.*))? title/i;

module.exports = Object.freeze(
  {
    patterns: {
      titlePattern,
    },
    minimumExample: {
      titlePattern: 'I see "value" in title',
    },
    examples: {
      titlePattern: [
        'I see "value" in title',
        'I see "value" in the title',
        'I see "value" in meta tag title',
      ],
    },
    usage: {
      titlePattern: ['Then', 'And'],
    }
  }
);
