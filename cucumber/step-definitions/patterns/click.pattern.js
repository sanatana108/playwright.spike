const clickPattern = /I click(?:(?:(?!\bframe|iframe\b).)*?)?\((?:((?!\bframe|iframe\b)[\.A-z0-9_\-=" #\/:\[\]]+)*)\)(?:(?:(?!\bframe|iframe\b).)*)?$/i;
const clickPatternIframe = /I click(?: (?:.*))? \((.*?)\) in (?:iframe|frame)(?: (?:.*?))?\((.*?)\)?$/i;

module.exports = Object.freeze(
  {
    patterns: {
      clickPattern,
      clickPatternIframe
    },
    minimumExample: {
      clickPattern: 'I click (CSS_SELECTOR)',
      clickPatternIframe: 'I click (CSS_SELECTOR) in iframe (CSS_SELECTOR)',
    },
    examples: {
      clickPattern: [
        'I click (CSS_SELECTOR)',
        'I click (CSS_SELECTOR) to do something with the webpage',
        'I click on fist item in search results (CSS_SELECTOR) to open a new page'
      ],

      clickPatternIframe: [
        'I click (CSS_SELECTOR) in iframe (CSS_SELECTOR)',
        'I click on button (CSS_SELECTOR) in iframe (CSS_SELECTOR)',
        'I click on video (CSS_SELECTOR) in frame (CSS_SELECTOR)',
        ]
    },
    usage: {
      clickPattern: ['When', 'And'],
      clickPatternIframe: ['When', 'And']
    }
  }
);
