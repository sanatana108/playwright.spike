
const elementNotInViewportPattern = /(.*)?\((.*)\) is not( (visible))? in( (the|a))? viewport/i
const elementInViewportPattern = /(.*)?\((.*)\) is( (visible))? in( (the|a))? viewport/i
const iSeeTextInViewportPattern = /I (see|expect|expect to see)( (text|content))? "(.*)"(\|(\w+))?(.*)? in( (a|the))? viewport/i;
const textInViewportPattern = /((text|content) )?"(.*)"(\|(\w+))? is in( (a|the))? viewport/i;
const iSeeElementInViewportPattern = /I (see|expect|expect to see)( (.*))? \((.*)\) in( (a|the))? viewport/i;

module.exports = Object.freeze(
  {
    patterns: {
      elementNotInViewportPattern,
      elementInViewportPattern,
      iSeeTextInViewportPattern,
      iSeeElementInViewportPattern,
      textInViewportPattern
    },
    minimumExample: {
      elementNotInViewportPattern: '(CSS_SELECTOR) is not in viewport',
      elementInViewportPattern: '(CSS_SELECTOR) is in viewport',
      iSeeTextInViewportPattern: 'I see text "value" in viewport',
      iSeeElementInViewportPattern: 'I see (CSS_SELECTOR) in viewport',
      textInViewportPattern: 'Text "value" is in viewport',
    },
    examples: {
      textInViewportPattern: [
        'Text "value" is in viewport',
        'Text "value" is in the viewport',
      ],
      elementNotInViewportPattern: [
        'Element description (CSS_SELECTOR) is not visible in the viewport',
        'Element (CSS_SELECTOR) is not in the viewport',
      ],
      elementInViewportPattern: [
        'Element description (CSS_SELECTOR) is in the viewport',
        'Element (CSS_SELECTOR) is in viewport'
      ],
      iSeeTextInViewportPattern: [
        'I see text "value" in viewport',
        'I see text "value" in the viewport',
        'I expect text "value" in the viewport',
        'I expect content "value" in the viewport',
        'I expect to see text "value" in the viewport',
        'I expect to see content "value" in the viewport',
      ],

      iSeeElementInViewportPattern: [
        'I see (CSS_SELECTOR) in viewport',
        'I expect (CSS_SELECTOR) in viewport',
        'I expect to see (CSS_SELECTOR) in viewport',
        'I see element description (CSS_SELECTOR) in the viewport',
        'I expect element description (CSS_SELECTOR) in the viewport',
        'I expect to see element description (CSS_SELECTOR) in the viewport',
      ]
    },
    usage: {
      elementNotInViewportPattern: ['Then', 'And'],
      elementInViewportPattern: ['Then', 'And'],
      iSeeTextInViewportPattern: ['Then', 'And'],
      iSeeElementInViewportPattern: ['Then', 'And'],
      textInViewportPattern: ['Then', 'And'],
    }
  }
);
