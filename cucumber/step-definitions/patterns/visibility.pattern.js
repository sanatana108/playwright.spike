const elementWithTextPattern = /I see(.*)?( \(.*\))?( with) (text|content) "(.*)"(\|(\w+))?$/i;
const contentIsInElementPattern = /I see (text|content) "(.*)"(\|(\w+))? in ((.*) )?\((.*)\)?/i;
const textIsPresentPattern = /I see (text|content) "(.*)"(\|(\w+))?$/i;

const contentIsNotPresentPattern = /^((Text|Content) )?"(.*)"(\|(\w+))? is not (present|visible)( (.*))?/i;

const contentIsPresentPattern = /^((Text|Content) )?"(.*)"(\|(.*))? is (present|visible)( (.*))?/i;
const elementIsNotVisiblePattern = /(.*)?\((.*)\) is not visible(.*)(?<!viewport)$/i;
const elementIsVisiblePattern = /(.*)?\((.*)\)(((?!(and text|and content| with text|with content)).)*)? is visible(.*)(?<!viewport)$/i;
const elementWithContentIsVisiblePattern = /(.*)?\((.*)\) (and|with) (content|text) "(.*)"(\|(.*))? is visible(.*)(?<!viewport)$/i;
const elementWithContentIsNotVisiblePattern = /(.*)?\((.*)\) (and|with) (content|text) "(.*)"(\|(.*))? is not visible(.*)(?<!viewport)$/i;
const waitForElementToBeVisiblePattern = /I wait(.*) \((.*)\) to (be|become) (visible)/i;
const waitForElementToBeAvailablePattern = /I wait(.*) \((.*)\) to( (be|become))? (loaded|load|available)/i;

module.exports = Object.freeze(
  {
    patterns: {
      elementWithTextPattern,
      contentIsInElementPattern,
      contentIsNotPresentPattern,
      elementIsNotVisiblePattern,
      elementIsVisiblePattern,
      elementWithContentIsVisiblePattern,
      elementWithContentIsNotVisiblePattern,
      waitForElementToBeVisiblePattern,
      waitForElementToBeAvailablePattern,
      contentIsPresentPattern,
      textIsPresentPattern,
    },
    minimumExample: {
      elementWithTextPattern: 'I see description with text "value"',
      contentIsInElementPattern: 'I see text "value" in (CSS_SELECTOR)',
      contentIsNotPresentPattern: '"value" is not present',
      elementIsNotVisiblePattern: '(CSS_SELECTOR) is not visible',
      elementIsVisiblePattern: '(CSS_SELECTOR) is visible',
      elementWithContentIsVisiblePattern: '(CSS_SELECTOR) and content "value" is visible',
      elementWithContentIsNotVisiblePattern: '(CSS_SELECTOR) and content "value" is not visible',
      waitForElementToBeVisiblePattern: 'I wait (CSS_SELECTOR) to be visible',
      waitForElementToBeAvailablePattern: 'I wait (CSS_SELECTOR) to be available',
      textIsPresentPattern: 'I see text "value"',
      contentIsPresentPattern: 'Text "value" is present',
    },
    examples: {
      textIsPresentPattern: [
        'I see text "value"',
        'I see content "value"',
      ],

      contentIsPresentPattern: [
        'Text "value" is present',
        'Text "value" is present',
        'Content "value" is visible',
        'Content "value" is present'
      ],
      elementWithTextPattern: [
        'I see element with text "value"|caseinsensitive',
        'I see search element with content "value"|keysensitive',
      ],
      contentIsInElementPattern: [
        'I see text "value" in description of element (CSS_SELECTOR)',
        'I see content "value" in (CSS_SELECTOR)',
      ],
      contentIsNotPresentPattern: [
        'Text "value" is not present',
        'Text "value" is not present on the visiting page',
        'Text "value" is not present on the page',
        'Content "value" is not present',
        'Content "value" is not present on the visiting page',
        'Content "value" is not present on the page',
      ],
      elementIsNotVisiblePattern: [
        'Element description (CSS_SELECTOR) is not visible',
        'Element description (CSS_SELECTOR) is not visible on the page'
      ],
      elementIsVisiblePattern: [
        'Element description (CSS_SELECTOR) is visible',
        'Element description (CSS_SELECTOR) is visible on the page'
      ],
      elementWithContentIsVisiblePattern: [
        '(CSS_SELECTOR) and content "value" is visible',
        '(CSS_SELECTOR) with content "value" is visible',
      ],
      elementWithContentIsNotVisiblePattern: [
        'Element description (CSS_SELECTOR) with content "value" is not visible',
        'Element description (CSS_SELECTOR) and content "value" is not visible',
        'Element description (CSS_SELECTOR) with text "value" is not visible',
        'Element description (CSS_SELECTOR) and text "value" is not visible',
      ],
      waitForElementToBeVisiblePattern: [
        'I wait for element (CSS_SELECTOR) to BE visible',
        'I wait for element (CSS_SELECTOR) to become visible',
        'I wait for element description (CSS_SELECTOR) to be visible',
        'I wait for element description (CSS_SELECTOR) to become visible',
      ],
      waitForElementToBeAvailablePattern: [
        'I wait for (CSS_SELECTOR) to be loaded',
        'I wait for (CSS_SELECTOR) to be load',
        'I wait for (CSS_SELECTOR) to be available',
        'I wait for (CSS_SELECTOR) to become loaded',
        'I wait for (CSS_SELECTOR) to load',
        'I wait for (CSS_SELECTOR) to become available',
        'I wait for optional text description (CSS_SELECTOR) to become available',
      ]
    },
    usage: {
      elementWithTextPattern: ['Then', 'And'],
      contentIsNotPresentPattern: ['Then', 'And'],
      elementIsNotVisiblePattern: ['Then', 'And'],
      elementIsVisiblePattern: ['Then', 'And'],
      elementWithContentIsVisiblePattern: ['Then', 'And'],
      elementWithContentIsNotVisiblePattern: ['Then', 'And'],
      waitForElementToBeVisiblePattern: ['Then', 'And'],
      waitForElementToBeAvailablePattern: ['Then', 'And'],
      contentIsInElementPattern: ['Then', 'And'],
      contentIsPresentPattern: ['Then', 'And'],
      textIsPresentPattern: ['Then', 'And']
    }
  }
);
