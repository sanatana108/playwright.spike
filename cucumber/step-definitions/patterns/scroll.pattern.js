const scrollTopOrBottomPattern = /I scroll to(?: (?:the|a))? (top|bottom)(?: (?:.*))?(?: (?:page|site|website))?/i;
const scrollToPattern = /I scroll to(?: (?:.*))? \((.*)\)(|(.*))?/i;

module.exports = Object.freeze(
  {
    patterns: {
      scrollTopOrBottomPattern,
      scrollToPattern,
    },
    minimumExample: {
      scrollTopOrBottomPattern: 'I scroll to bottom of page',
      scrollToPattern: 'I scroll to (CSS_SELECTOR)'
    },
    examples: {
      scrollTopOrBottomPattern: [
        'I scroll to the bottom of the page',
        'I scroll to the top of the page',
        'I scroll to top of the site',
        'I scroll to top of the website',
        'I scroll to bottom of the site',
        'I scroll to bottom of the website',
      ],
      scrollToPattern: [
        'I scroll to the search field (CSS_SELECTOR)',
        'I scroll to (CSS_SELECTOR)'
      ]
    },
    usage: {
      scrollToPattern: ['When', 'And'],
      scrollTopOrBottomPattern: ['When', 'And']
    }
  }
);
