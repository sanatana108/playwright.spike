const pageScreenShootPattern =  /(?: I)?Take(?: (?:a|the))?(?: (fullpage|full page))? screenshot(?:\|(?:timeout):(\d+))?(?:(?!\(|\))(?:["\.A-z0-9_\-= #\/:\[\]]+)?"(.*?)")?(?:"([A-z0-9_\- \[\]]\w+)")?(?:\|timeout:(\d+))?(?:(?:(?!\(|\)|").)*)?$/i;
const screenshotOfElementPattern = /(?: I)?Take(?: (?:a| the))? screenshot (?:(?:(?!fullpage|full page).)*?)?(?:(?!\")(?:["\.A-z0-9_\-= #\/:\[\]]+)?\((.*?)\))(?:.*?)(?:"(.*?)")?$/i;
const pageSnapShotPattern = /(?: I)?Take(?: (?:a|the))?(?: (fullpage|full page))? snapshot(?:\|(?:timeout):(\d+))?(?:(?!\(|\))(?:["\.A-z0-9_\-= #\/:\[\]]+)?"(.*?)")?(?:"([A-z0-9_\- \[\]]\w+)")?(?:\|timeout:(\d+))?(?:(?:(?!\(|\)|").)*)?$/i;
const snapshotOfElementPattern = /(?: I)?Take(?: (?:a| the))? snapshot (?:(?:(?!fullpage|full page).)*?)?(?:(?!\")(?:["\.A-z0-9_\-= #\/:\[\]]+)?\((.*?)\))(?:.*?)(?:"(.*?)")?$/i;
const compareScreenShotPattern = /Compare(?: (?:screenshot|it))?(?: with)?(?: (?:a|the))? (?:snapshot )?"(.*?)"$/i;

module.exports = Object.freeze(
  {
    patterns: {
      pageScreenShootPattern,
      snapshotOfElementPattern,
      compareScreenShotPattern,
      pageSnapShotPattern,
      screenshotOfElementPattern
    },
    minimumExample: {
      pageScreenShootPattern: 'Take screenshot',
      pageSnapShotPattern: 'Take fullpage snapshot',
      snapshotOfElementPattern: 'Take snapshot (CSS_SELECTOR)',
      screenshotOfElementPattern: 'Take screenshot (CSS_SELECTOR)',
      compareScreenShotPattern: 'Compare screenshot with "SNAPSHOT_NAME"',
    },
    examples: {
      pageScreenShootPattern: [
        'I take a screenshot',
        'I take the screenshot',
        'I take screenshot',
        'Take a screenshot',
        'Take the screenshot',
        'Take screenshot',
        'Take a fullpage screenshot',
        'Take a full page screenshot'
      ],

      pageSnapShotPattern: [
        'Take snapshot',
        'Take fullpage snapshot',
        'I take a fullpage snapshot|timeout:1000',
        'Take snapshot "NAME"',
        'Take fullpage snapshot "NAME"',
        'Take full page snapshot "NAME"',
        'I take a fullpage snapshot|timeout:1000 "NAME"',
        'I take fullpage snapshot with file name "NAME"|timeout:1000 more description ',
      ],

      snapshotOfElementPattern: [
        'Take a snapshot of element (CSS_SELECTOR)',
        'Take snapshot (CSS_SELECTOR)',
        'Take snapshot of search form (CSS_SELECTOR) with name "SNAPSHOT_NAME"',
      ],

      compareScreenShotPattern: [
        'Compare screenshot with "SNAPSHOT_NAME"',
        'Compare it with snapshot "SNAPSHOT_NAME"',
        'Compare it with a snapshot "SNAPSHOT_NAME"',
        'Compare it with "SNAPSHOT_NAME"',
        'Compare screenshot "SNAPSHOT_NAME"',
      ],

      screenshotOfElementPattern: [
        'Take a screenshot of search form (CSS_SELECTOR)',
        'Take screenshot (CSS_SELECTOR)',
        'Take screenshot (CSS_SELECTOR) with a name "SCREENSHOT_NAME"',
      ]
    },
    usage: {
      pageScreenShootPattern: ['Then', 'And'],
      pageSnapShotPattern: ['Then', 'And'],

      snapshotOfElementPattern: ['Then', 'And'],
      screenshotOfElementPattern: ['Then', 'And'],

      compareScreenShotPattern: ['Then', 'And'],
    }
  }
);
