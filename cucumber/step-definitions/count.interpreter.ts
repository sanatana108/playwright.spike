import { Then } from '@cucumber/cucumber';
import { getMatchRegex } from '../helpers/textOptions.helper';
import { getCorrectCondition } from '../helpers/countCondition.helper';
import { Page } from 'playwright';

const {
    countPattern,
} = require('./patterns/count.pattern').patterns;

// /I expect(.*(at least|at most|only|not more))?( (.*))? (\d+)( (.*))?\((.*)\)/i;
Then(countPattern, async function(countCondition: string, _, expectedCount: number, _2, selector: string) {
    const condition = getCorrectCondition(countCondition);
    const maxAttempts = 20;
    let attempt = 1;
    const page = this.page;

    const countElements = async () : Promise<Promise<any> | number> => {
        const timeoutMilliSeconds = 500;

        await page.waitForSelector(`${selector}`);
        const tempCount = await page.locator(`${selector}`).count();

        const formula = `${tempCount} ${condition} ${expectedCount}`;

        if (eval(formula)) {
            return tempCount;
        }

        attempt += 1;
        if (attempt < maxAttempts) {
            await page.waitForTimeout(timeoutMilliSeconds);
            return countElements();
        }

        return tempCount;
    };

    const actualCount = await countElements();
    this.expect(eval(`${actualCount} ${condition} ${expectedCount}`)).toBe(true);
});

