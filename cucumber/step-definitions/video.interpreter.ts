import { Then } from '@cucumber/cucumber';

const {
    videoInIframeIsPlayingPattern,
    videoInIframeIsNotPlayingPattern
} = require('./patterns/video.pattern').patterns;

Then(videoInIframeIsPlayingPattern, async function(videoSelector: string, iframeSelector: string) {
    //be mindful of $ selector
    const iframeHandle = await this.page.$(`${iframeSelector}`);
    const videoIframe = await iframeHandle.contentFrame();
    await videoIframe.waitForSelector(`${videoSelector}`);
    const video = await videoIframe.locator(`${videoSelector}`);

    this.expect(await video.count()).toBe(1);
    await this.expect(video.isVisible()).toBeTruthy();
    await this.expect(video).isVideoPlaying();
    iframeHandle.dispose();
});

Then(videoInIframeIsNotPlayingPattern, async function(videoSelector: string, iframeSelector: string) {
    const iframeHandle = await this.page.$(`${iframeSelector}`);
    const videoIframe = await iframeHandle.contentFrame();
    await videoIframe.waitForSelector(`${videoSelector}`);
    const video = await videoIframe.locator(`${videoSelector}`);

    this.expect(await video.count()).toBe(1);

    await this.expect(video.isVisible()).toBeTruthy();
    await this.expect(video).isVideoNotPlaying();
    iframeHandle.dispose();
});
