import { Given, Then } from '@cucumber/cucumber';
import { isUrl } from '../helpers/urlHelper.helper';
import { getMatchRegex } from '../helpers/textOptions.helper';

const {
  openUrlPattern,
  urlShouldBePattern,
  urlShouldIncludePattern
} = require('./patterns/browserNavigation.pattern').patterns;

Given(openUrlPattern, async function (url: string) {
  await this.page.goto(url, { waitUntil: 'networkidle' });
});

Then(urlShouldBePattern, async function(expectedUrl: string) {
  const url = await this.page.url();

  if (isUrl(expectedUrl)) {
    this.expect(url).toBe(expectedUrl);
    return;
  }

  const urlParts = new URL(url);
  this.expect(`${urlParts.pathname}${urlParts.search}`).toBe(expectedUrl);
});

Then(urlShouldIncludePattern, async function(expectedUrl: string) {
  const url = await this.page.url();
  await this.expect(url).toMatch(getMatchRegex(expectedUrl, false));
});
