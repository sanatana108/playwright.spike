import { Then } from '@cucumber/cucumber';
import { getMatchRegex } from '../helpers/textOptions.helper';

const { titlePattern } = require('./patterns/meta.pattern').patterns;

Then(titlePattern, async function (expectedTitle: string, textOptions: string, _) {
    const { page } = this;
    const actualTitle = await page?.title();
    this.expect(actualTitle).toMatch(getMatchRegex(expectedTitle, textOptions));
});
