import { Then } from '@cucumber/cucumber';
const {
    inputPattern,
    enterPattern,
    inputValuePattern
} = require('./patterns/input.pattern').patterns;
import { escapeRegexString, isMatchCase } from '../helpers/textOptions.helper';

Then(inputPattern, async function(textValue: string, selector: string) {
    await this.page.fill(selector, textValue);
});

Then(enterPattern, async function(selector) {
    await this.page.press(`${selector}`, 'Enter');
});

Then(inputValuePattern, async function(_, _2, selector: string, expectedValue: string, matchOptions: string) {
    const contentValue = await this.page.inputValue(`${selector}`);
    const matchCase = isMatchCase(matchOptions);
    const escapedExpectedValue = escapeRegexString(expectedValue);

    /// TODO: Complete this...
    //this.expect(contentValue).toMatch(new RegExp(^${escapedExpectedValue}$, matchCase ? '' : 'i'));
});

