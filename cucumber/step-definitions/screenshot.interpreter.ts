import { Then } from '@cucumber/cucumber';
import fs from 'fs';
import { ensureDir } from 'fs-extra';
const path = require('path');

import {
    isFullPage,
    screenshotScrollFullPage,
    getScreenShotBaseFilename,
    safeFileNameCharacters,
} from '../helpers/screenshot.helper';

const fullPageScreenTimeoutMs = 1000 * 60 * 5;

const {
    pageScreenShootPattern,
    pageSnapShotPattern,
    snapshotOfElementPattern,
    compareScreenShotPattern,
// eslint-disable-next-line @typescript-eslint/no-var-requires
} = require('./patterns/screenshot.pattern').patterns;

const screenshotDirectory = 'results/images-screenshots/';
const snapshotDirectory = 'images/snapshots/';
const differenceDirectory = 'results/images-difference/';
const overrideScreenShots = true;

Then(pageScreenShootPattern, async function(wholePage: string, timeout1: number|null, screenName: string, timeout2: number|null, timeout3: number|null) {
    const fullPage = isFullPage(wholePage);

    const screenshotName = await getScreenShotBaseFilename(this, { name: screenName });
    const screenShotFilename = `${screenshotDirectory}${screenshotName}`;
    ensureDir(screenshotDirectory);

    if (fullPage) {
        this.setTimeout(fullPageScreenTimeoutMs);
        const timeoutMs = timeout1 || timeout2 || timeout3;
        await screenshotScrollFullPage(this.page, screenShotFilename, overrideScreenShots, timeoutMs);
        this.resetTimeout();
    } else {
        await takeScreenShot(this.page, screenShotFilename);
    }

    this.lastScreenShot = {
        filename: screenShotFilename,
    }
});

Then(pageSnapShotPattern, async function(wholePage: string, timeout1: number|null, snapshotName: string, _: string, timeout2: number|null) {
    ensureDir(snapshotDirectory);
    const fullPage = isFullPage(wholePage);
    let snapshotFilename = await getScreenShotBaseFilename(this, { name: snapshotName });
    snapshotFilename = `${snapshotDirectory}${snapshotFilename}`;

    if (fullPage) {
        this.setTimeout(fullPageScreenTimeoutMs);
        const timeoutMs = timeout1 || timeout2 || 0;
        await screenshotScrollFullPage(this.page, snapshotFilename, true, timeoutMs);
        this.resetTimeout();
    } else {
        await takeScreenShot(this.page, snapshotFilename);
    }
});

Then(compareScreenShotPattern, async function(snapshotName: string) {
    ensureDir(differenceDirectory);
    const snapshotFileName = await getScreenShotBaseFilename(this, { name: snapshotName });
    const compareWithFile = `${snapshotDirectory}${snapshotFileName}`;

    this.expect(this.lastScreenShot.filename).toBeTruthy();

    try{
        this.expect(fs.existsSync(compareWithFile)).toBeTruthy();
    } catch (e) {
        throw new Error(`Snapshot "${compareWithFile}" file doesn't exist`);
    }

    const baseTestName = safeFileNameCharacters(`${this.testBaseName}`);
    const originalScreenShotBaseName = path.parse(this.lastScreenShot.filename).name;
    const differenceFile = `${differenceDirectory}${baseTestName}${originalScreenShotBaseName}-diff.png`;
    await this.expect(this.lastScreenShot.filename).toMatchMySnapshot(compareWithFile, {
        filename: differenceFile,
        threshold: 0.5,
        compareOnly: true,
        override: true,
    });
});

Then(snapshotOfElementPattern, async function(selector: string, name: string) {
    ensureDir(snapshotDirectory);
    await this.page.waitForSelector(selector, { waitUntil: 'networkidle' });
    const snapshotName = await getScreenShotBaseFilename(this, { selector: selector, name: name });
    const snapshotFilename = `${snapshotDirectory}${snapshotName}`;
    const element = await this.page.$(selector);
    await takeScreenShot(element, snapshotFilename);
});

const takeScreenShot = async (element: any, filename: string) => {
    await element.screenshot({ path: filename }, { waitUntil: 'networkidle' });
}
