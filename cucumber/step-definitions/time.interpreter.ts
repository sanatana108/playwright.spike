import { Then } from '@cucumber/cucumber';
import { getMatchRegex } from '../helpers/textOptions.helper';

const {
    waitPattern
} = require('./patterns/time.pattern').patterns;

// const waitPattern = /I wait ([0-9]+\.?[0-9]*|\.[0-9]+) second(s)?/i
Then(waitPattern, async function(seconds: number, _1) {
    await this.page?.waitForTimeout(seconds * 1000);
});
