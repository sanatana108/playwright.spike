import { When } from '@cucumber/cucumber';

const {
    clickPattern,
    clickPatternIframe,
} = require('./patterns/click.pattern').patterns;

When(clickPattern, async function (selector) {
    await this.page.click(`${selector}`);
});

//const clickPatternIframe = /I click( (.*))? \((.*)\) in (iframe|frame)( (.*?))?\((.*)\)?/i;
When(clickPatternIframe, async function(elementSelector: string, iframeSelector: string) {
    const iframeHandle = await this.page.$(`${iframeSelector}`);
    const iframe = await iframeHandle.contentFrame();
    const element = await iframe.locator(`${elementSelector} >> visible=true`);
    this.expect(await element.count()).toBe(1);
    await this.expect(element.isVisible()).toBeTruthy();

    await element.click();

});