import { When } from '@cucumber/cucumber';

const {
    scrollTopOrBottomPattern,
    scrollToPattern
} = require('./patterns/scroll.pattern').patterns;

When(scrollTopOrBottomPattern, async function (topOrBottom: string) {
    const scrollBrowserFunction = `
    if (typeof customTestScrollNameSpace === 'undefined') {
        var customTestScrollNameSpace = customTestScrollNameSpace || {
          toTheTop: function() {
            var scrollingElement = (document.scrollingElement || document.body);
            window.scrollTo({ top: 0, behavior: 'smooth' });
             return customTestScrollNameSpace.sleep(750).then(() => {
                if (!customTestScrollNameSpace.isAtTheTop()) {
                  return customTestScrollNameSpace.toTheTop();
                }
                return true;
            });
          },
          toTheBottom: function() {
            var scrollingElement = (document.scrollingElement || document.body);
            window.scrollTo({ top: scrollingElement.scrollHeight, behavior: 'smooth' });
            return customTestScrollNameSpace.sleep(750).then(() => {
                if (!customTestScrollNameSpace.isAtTheBottom()) {
                  return customTestScrollNameSpace.toTheBottom();
                }
                return true;
            });
          },
          sleep: function(ms) {
            return new Promise((resolve, reject) => {
              setTimeout(resolve, ms);
            });
          },
          isAtTheBottom: function() {
            return (window.innerHeight + (window.pageYOffset || window.scrollY)) >= document.body.offsetHeight - 2;
          },
          isAtTheTop: function() {
            return  (window.pageYOffset || window.scrollY) <= 0;
          }
        };
    }
    `;

    if (topOrBottom === 'bottom') {
      await this.page?.evaluate(`${scrollBrowserFunction}customTestScrollNameSpace.toTheBottom();`);
    } else if (topOrBottom === 'top') {
      await this.page?.evaluate(`${scrollBrowserFunction}customTestScrollNameSpace.toTheTop();`);
    }
});

When(scrollToPattern, async function(selector: string) {
    await this.page.locator(`${selector}`).scrollIntoViewIfNeeded();
});
