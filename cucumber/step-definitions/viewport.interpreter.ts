import { Then, Given } from '@cucumber/cucumber';
import { getMatchRegex } from '../helpers/textOptions.helper';
import { Page } from 'playwright';

const {
    iSeeTextInViewportPattern,
    iSeeElementInViewportPattern,
    textInViewportPattern,
    elementInViewportPattern,
    elementNotInViewportPattern,
} = require('./patterns/viewport.pattern').patterns;


Then(iSeeTextInViewportPattern, async function (_1, _2, content: string, matchCaseOption: string, _5, _6) {
    await testTestInViewport(this.expect, this.page, content, matchCaseOption);
});

Then(iSeeElementInViewportPattern, async function(_, _1, selector, _2) {
    const element = await this.page?.locator(`${selector} >> visible=true >> nth=0`);
    await this.expect(element).isVisibleInViewport(this.page, true);
});

Then(textInViewportPattern, async function(_1, content: string, matchCaseOption: string, _2) {
    await testTestInViewport(this.expect, this.page, content, matchCaseOption);
});

// /(.*)?\((.*)\) is not( (visible))? in( (the|a))? viewport/i
Then(elementNotInViewportPattern, async function(_1, selector: string, _2, _3) {
    const element = await this.page?.locator(`${selector} >> visible=true >> nth=0`).first();
    await this.expect(element).isNotVisibleInViewport(this.page);
});

// /(.*)?\((.*)\) is( (visible))? in( (the|a))? viewport/i
Then(elementInViewportPattern, async function(_1, selector: string, _2, _3) {
    const element = await this.page?.locator(`${selector} >> visible=true >> nth=0`).first();
    await this.expect(element).isVisibleInViewport(this.page, true);
});

const testTestInViewport = async function(expect: any, page: Page, content: string, matchCaseOption: string) {
    const searchPattern = getMatchRegex(content, matchCaseOption).toString();
    const textElement = await page?.locator(`text=${searchPattern} >> visible=true`).first();
    const textElements = await page?.locator(`text=${searchPattern} >> visible=true`).first();
    console.log(textElements);

    expect(await textElement.count()).toBe(1);
    await expect(textElement).isVisibleInViewport(page, true);
}
