Feature: Google

  Background:
    Given I open page "https://www.google.com/search/howsearchworks/?fg=1" in a browser

  @desktop
  Scenario: Google how search works
    Then I take a fullpage screenshot "google-how-search-works-desktop-1"|timeout:1000
    Then Compare it with a snapshot "google-how-search-works-desktop-1"

    When I click on I agree button (.cookieBarButton.cookieBarConsentButton)
    ## This will take a screenshot and will be saved in results/images-screenshots
    ## (snapshots are saved in images in root)
    Then Take a fullpage screenshot "google-how-search-works-desktop-2"|timeout:1000s

    ## This will only compare screenshot and snapshot and the result will be saved in results/images-difference
    ## The name is set in snapshot feature files
    Then Compare it with a snapshot "google-how-search-works-desktop-2"

  @mobile
  Scenario: Google mobile home page
    Then I take a fullpage screenshot "google-how-search-works-1"|timeout:1000
    And Compare it with a snapshot "google-how-search-works-1"

    Then I click on I agree button (.cookieBarButton.cookieBarConsentButton)
    Then I take a fullpage screenshot "google-how-search-works-2"|timeout:1000
    And Compare it with a snapshot "google-how-search-works-2"
