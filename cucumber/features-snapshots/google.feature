Feature: Google

  @desktop
  Scenario: Google desktop home page
    Given I visit page "https://www.google.com/" in a browser
    When I click on I agree button (#L2AGLb)
    Then Take a snapshot of search form (form[action="/search"])
    And Take fullpage snapshot|timeout:1000
    And Take fullpage snapshot|timeout:0

  @mobile
  Scenario: Google mobile home page
    Given I visit page "https://www.google.com/" in a browser
    Then I take a snapshot of search form (form[action="/search"])
    And I take fullpage snapshot "mobile-homepage"|timeout:1000
    Then I take fullpage snapshot "mobile-homepage-2"|timeout:2000

