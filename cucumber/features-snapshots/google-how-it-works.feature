Feature: Google

  Background:
    Given I open page "https://www.google.com/search/howsearchworks/?fg=1" in a browser

  @desktop
  Scenario: Google how search works
    Then I take a fullpage snapshot "google-how-search-works-desktop-1"|timeout:1000
    When I click on I agree button (.cookieBarButton.cookieBarConsentButton)
    Then Take a fullpage snapshot "google-how-search-works-desktop-2"|timeout:50

  @mobile
  Scenario: Google mobile home page
    Then I take a fullpage snapshot "google-how-search-works-1"|timeout:1000
    Then I click on I agree button (.cookieBarButton.cookieBarConsentButton)
    Then I take a fullpage snapshot "google-how-search-works-2"|timeout:1000

    #Then I take a fullpage snapshot "google-mobile-homepage-2"|timeout:50
    #Then I take a snapshot of search form (form[action="/search"]) "google-mobile-search-form"
    #When I enter "London" to input search field (input[title=Search])

    #Then I take fullpage snapshot "google-mobile-homepage-3"|timeout:10


