import {
    Before, After, BeforeAll, AfterAll, Status, setDefaultTimeout
} from '@cucumber/cucumber';
import { ensureDir } from 'fs-extra';
import { matchers } from 'expect-playwright';
import { ICustomWorld } from './helpers/customWorld.helper';
import { browserOptions } from './helpers/config.helper';
import { ITestCaseHookParameter } from '@cucumber/cucumber/lib/support_code_library_builder/types';
import { customMatchers } from './helpers/customAssertions';
import { getRequestedDevice, getDefaultViewportSize } from './helpers/device.helper';
import { sep } from 'path';

import {
    chromium,
    ChromiumBrowser,
    firefox,
    FirefoxBrowser,
    webkit,
    WebKitBrowser,
} from 'playwright';

import expect from 'expect';
expect.extend(matchers);
expect.extend(customMatchers);

let browser: ChromiumBrowser | FirefoxBrowser | WebKitBrowser;
const tracesDir = 'results/traces';
const screenShotDir = 'results/screenshots';
const videoDir = 'results/videos';

declare global {
    const browser: ChromiumBrowser | FirefoxBrowser | WebKitBrowser;
    const page: Promise<ICustomWorld>;
}

setDefaultTimeout(process.env.PWDEBUG ? -1 : 60 * 1000);

BeforeAll(async function () {
    const launchOptions = { ...browserOptions };

    switch (process.env.PLAYWRIGHT_BROWSER) {
        case 'firefox':
            browser = await firefox.launch(browserOptions);
            process.env.ACTIVE_BROWSER = process.env.BROWSER;
            break;

        case 'webkit':
            browser = await webkit.launch(launchOptions);
            process.env.ACTIVE_BROWSER = process.env.BROWSER;
            break;

        case 'chrome':
            browser = await chromium.launch({ ...launchOptions, channel: 'chrome' });
            process.env.ACTIVE_BROWSER = process.env.BROWSER;
            break;

        case 'edge':
            browser = await chromium.launch({...launchOptions, channel: 'msedge' });
            process.env.ACTIVE_BROWSER = process.env.BROWSER;
            break;

        case 'chromium':
        default:
            browser = await chromium.launch(launchOptions);
            process.env.ACTIVE_BROWSER = 'chromium';
            break;
    }

    const context = await browser.newContext();
    await context.clearCookies();
    await ensureDir(tracesDir);
    await ensureDir(screenShotDir);
    await ensureDir(videoDir);
});

Before(async function (this: ICustomWorld) {
    const device = getRequestedDevice();
    this.device = device;
});

Before({ tags: '@ignore' }, async function () {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    return 'skipped' as any;
});

Before({ tags: '@skip' }, async function () {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    return 'skipped' as any;
});

Before({ tags: '@debug' }, async function (this: ICustomWorld) {
    this.debug = true;
});

Before({ tags: '@mobile' }, async function (this: ICustomWorld) {
    if (!this.device.isMobile) {
        return 'skipped' as any;
    }
});

Before({ tags: '@desktop' }, async function (this: ICustomWorld) {
    if (this.device.isMobile) {
        return 'skipped' as any;
    }
});

Before(function (this: ICustomWorld, { pickle, gherkinDocument }: ITestCaseHookParameter) {
    let featureFile = ((gherkinDocument.uri ? gherkinDocument.uri : ''));
    featureFile = featureFile.split(sep.toString())
        .slice(2).join('-')
        .trim()
        .replace(/\.feature$/i, '');
    this.testBaseName = (`${featureFile}-${pickle.name}`).replace(/\W/g, '-').toLowerCase();
});

Before(async function (this: ICustomWorld, { pickle }: ITestCaseHookParameter) {

    const time = new Date().toISOString().split('.')[0];
    this.testName = pickle.name.replace(/\W/g, '-') + '-' + time.replace(/:|T/g, '-');
    this.browserName = process.env.ACTIVE_BROWSER ? process.env.ACTIVE_BROWSER : 'chromium';
    this.stepNumber = 0;

    const contextOptions = {
        acceptDownloads: true,
        recordVideo: process.env.PWVIDEO ? { dir: videoDir } : undefined,
        ...getDefaultViewportSize(),
        ...this.device
    };

    this.context = await browser.newContext({
        ...contextOptions,
    });

    await this.context.tracing.start({ screenshots: true, snapshots: true });

    this.lastScreenShot = {};
    this.page = await this.context.newPage();
    this.browser = browser;
    this.expect = expect;
    this.feature = pickle;

    // @ts-ignore
    this.page.on('console', async (msg: any) => {
        if (msg.type() === 'log') {
            await this.attach(msg.text());
        }
    });

    this.setTimeout = function(value: number) {
        if (this.page) {
            this.page.setDefaultTimeout(value);
        }
    }

    this.resetTimeout = function() {
        if (this.page) {
            this.page.setDefaultTimeout(30000);
        }
    }
});

After(async function (this: ICustomWorld, { result }: ITestCaseHookParameter) {
    if (result) {
        await this.attach(`Status: ${result?.status}. Duration:${result.duration?.seconds}s`);

        if (result.status !== Status.PASSED) {
            const image = await this.page?.screenshot();
            image && (await this.attach(image, 'image/png'));
            await this.context?.tracing.stop({ path: `${tracesDir}/${this.testName}-trace.zip` });
        }
    }

    await this.page?.close();
    await this.context?.close();
});

AfterAll(async function (this: ICustomWorld) {
    await browser.close();
});
