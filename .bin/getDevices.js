const {
  devices,
} = require('playwright');

const deviceRegex = /device=(.*?)\b$/i;

const isDeviceRequested = () => {
  for (const key in process.argv) {
    if (deviceRegex.test(process.argv[key])) {
      return true;
    }
  }
  return false;
}

const getRequestedDeviceId = () => {
  let deviceId = null;
  for (const key in process.argv) {
    if (deviceRegex.test(process.argv[key])) {
      const match = process.argv[key].match(deviceRegex);
      if (match && match[1]) {
        deviceId = match[1];
        break;
      }
    };
  }
  return deviceId;
}

const getRequestedDevice = () => {
  let device = null;
  const deviceId = getRequestedDeviceId();

  if (!deviceId) {
    return device;
  }

  if (devices[deviceId]) {
    return { ...device, ...devices[deviceId], name: deviceId };
  }

  return device;
}

const getListOfDevices = () => {
  console.log('List of supported devices:');
  console.log('##########################');
  supportedDevices.forEach((item) => {
    console.log(item);
  });
  console.log('##########################');
}

const supportedDevices = Object.keys(devices);

if (isDeviceRequested()) {
  const device = getRequestedDevice();
  if (device) {
    console.log(device);
  } else {
    console.log('Request device is not supported!');
  }
} else {
  getListOfDevices();
}

