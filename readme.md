# Playwright prototype with cucumber runner

## Requirements:

- Node v14
- npm
- Optionally git bash (for running CLI commands)

## First run:

This will install all required npm packages and cypress

- **npm run install:full**

Alternatively you can run:

- **npm install**
  and
- **npx playwright install**

## Playwright tests:
This will execute all .feature files in cucumber/features

- **npm run test** (this will run test in default chromium browser)
- **npm run test:chrome**
- **npm run test:firefox**
- **npm run test:edge**
- **npm run test:webkit**

## Playwright snapshots
*To create visual snapshots for comparison you can run:*
This will read all the files from cucumber/features-snapshots

- **npm run test:snapshots** (this will run test in default chromium browser)
- **npm run test:snapshots:chrome**
- **npm run test:snapshots:firefox**
- **npm run test:snapshots:edge**
- **npm run test:snapshots:webkit**

## Device emulation

To get a list of all playwright supported devices you can run:

- **npm run devices**

To run test of snapshot creation run:
- **npm run test device="{device_name}"**
- **npm run test:chrome device="{device_name}"**
- **npm run test:webkit device="{device_name}"**

To get information about device run:

- **npm run devices device="{device_name}"


### Supported devices

- Blackberry PlayBook
- Blackberry PlayBook landscape
- BlackBerry Z30
- BlackBerry Z30 landscape
- Galaxy Note 3
- Galaxy Note 3 landscape
- Galaxy Note II
- Galaxy Note II landscape
- Galaxy S III
- Galaxy S III landscape
- Galaxy S5
- Galaxy S5 landscape
- Galaxy S8
- Galaxy S8 landscape
- Galaxy S9+
- Galaxy S9+ landscape
- Galaxy Tab S4
- Galaxy Tab S4 landscape
- iPad (gen 6)
- iPad (gen 6) landscape
- iPad (gen 7)
- iPad (gen 7) landscape
- iPad Mini
- iPad Mini landscape
- iPad Pro 11
- iPad Pro 11 landscape
- iPhone 6
- iPhone 6 landscape
- iPhone 6 Plus
- iPhone 6 Plus landscape
- iPhone 7
- iPhone 7 landscape
- iPhone 7 Plus
- iPhone 7 Plus landscape
- iPhone 8
- iPhone 8 landscape
- iPhone 8 Plus
- iPhone 8 Plus landscape
- iPhone SE
- iPhone SE landscape
- iPhone X
- iPhone X landscape
- iPhone XR
- iPhone XR landscape
- iPhone 11
- iPhone 11 landscape
- iPhone 11 Pro
- iPhone 11 Pro landscape
- iPhone 11 Pro Max
- iPhone 11 Pro Max landscape
- iPhone 12
- iPhone 12 landscape
- iPhone 12 Pro
- iPhone 12 Pro landscape
- iPhone 12 Pro Max
- iPhone 12 Pro Max landscape
- JioPhone 2
- JioPhone 2 landscape
- Kindle Fire HDX
- Kindle Fire HDX landscape
- LG Optimus L70
- LG Optimus L70 landscape
- Microsoft Lumia 550
- Microsoft Lumia 550 landscape
- Microsoft Lumia 950
- vMicrosoft Lumia 950 landscape
- Nexus 10
- Nexus 10 landscape
- Nexus 4
- Nexus 4 landscape
- Nexus 5
- Nexus 5 landscape
- Nexus 5X
- Nexus 5X landscape
- Nexus 6
- Nexus 6 landscape
- Nexus 6P
- Nexus 6P landscape
- Nexus 7
- Nexus 7 landscape
- Nokia Lumia 520
- Nokia Lumia 520 landscape
- Nokia N9
- Nokia N9 landscape
- Pixel 2
- Pixel 2 landscape
- Pixel 2 XL
- Pixel 2 XL landscape
- Pixel 3
- Pixel 3 landscape
- Pixel 4
- Pixel 4 landscape
- Pixel 4a (5G)
- Pixel 4a (5G) landscape
- Pixel 5
- Pixel 5 landscape
- Moto G4
- Moto G4 landscape
- Desktop Chrome HiDPI
- Desktop Edge HiDPI
- Desktop Firefox HiDPI
- Desktop Safari
- Desktop Chrome
- Desktop Edge
- Desktop Firefox

## Simple documentation
Run local node/express server

- **npm run docs**

Go to:
- http://localhost:3001

or to get Json object
- http://localhost:3001/api 

## When adding new pattern verify examples and possible regex classes with:

- **npm run test:jest**

## Test tags
In your tests you can use the following tags:

- @mobile, @desktop to run scenarios only on that particular device
- @ignore, @skip will not run the test on any device
